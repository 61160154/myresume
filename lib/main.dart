import 'package:flutter/material.dart';

Column _buildImagesColumn(Color color, String label) {
  return Column(
    children: [
      Container(
          child: Text(
        label,
        style: TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.w400,
          height: 1.5,
          color: color,
        ),
      ))
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget profileSection = Container(
      padding: const EdgeInsets.only(top: 15, left: 40, right: 40),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                'PLOYRUNG DUSARUX',
                style: TextStyle(
                    fontSize: 20, fontWeight: FontWeight.bold, height: 2.0),
              ),
              Text(
                '2609 Charoen Krung Rd., Bang Kho Laem Area, Bangkok 10120, Thailand',
                style: TextStyle(fontSize: 12, height: 1.5),
                softWrap: true,
              ),
              Text(
                'Contact no.: +66 94 325 0522',
                style: TextStyle(fontSize: 12, height: 1.5),
              ),
              Text(
                'Email: p.dusarux@gmail.com, 61160154@go.buu.ac.th',
                style: TextStyle(fontSize: 12, height: 1.5),
              ),
              Text(
                'Birthday: 22 JUNE 2000',
                style: TextStyle(fontSize: 12, height: 1.5),
              )
            ],
          )),
        ],
      ),
    );
    Widget educationSection = Container(
      padding: const EdgeInsets.only(top: 15, left: 40, right: 40),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Education',
                style: TextStyle(
                    fontSize: 15, fontWeight: FontWeight.bold, height: 3.0),
              ),
              Text(
                'Burapha University | Computer Science, B.Sc',
                style: TextStyle(fontSize: 12),
              ),
              Text(
                'Relevant Coursework',
                style: TextStyle(
                    fontSize: 12, height: 1.5, color: Colors.grey[600]),
              ),
              Container(
                child: Row(
                  children: [
                    Icon(Icons.arrow_right),
                    Expanded(
                      child: Text(
                        ' Algorithm and Data Structures, Web Programming, Mobile Programming',
                        style: TextStyle(
                            fontSize: 10, height: 0, color: Colors.grey[600]),
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
          Text(
            'JUNE 2018 - Present, Chon Buri',
            style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
    Widget skillsSection = Container(
      padding: const EdgeInsets.only(top: 15, left: 40, right: 40),
      child: Row(
        children: [
          Container(
            width: 50,
            height: 100,
            child: Text('Skills',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
          ),
          Expanded(
              child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      Image.asset(
                        'images/html5.png',
                        width: 60,
                        height: 60,
                      ),
                      _buildImagesColumn(color, 'HTML 5')
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/css32.png',
                        width: 60,
                        height: 60,
                      ),
                      _buildImagesColumn(color, 'CSS 3')
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/js.png',
                        width: 60,
                        height: 60,
                      ),
                      _buildImagesColumn(color, 'Javascript')
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      Image.asset(
                        'images/gitlab.png',
                        width: 60,
                        height: 60,
                      ),
                      _buildImagesColumn(color, 'GITLAB')
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/vue.png',
                        width: 60,
                        height: 60,
                      ),
                      _buildImagesColumn(color, 'VUE.js')
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/swift.png',
                        width: 60,
                        height: 60,
                      ),
                      _buildImagesColumn(color, 'swift')
                    ],
                  )
                ],
              ),
            ],
          )),
        ],
      ),
    );
    Widget extraCurricularSection = Container(
      padding: const EdgeInsets.only(top: 15, left: 40, right: 40),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Extracurricular Activities',
                style: TextStyle(
                    fontSize: 15, fontWeight: FontWeight.bold, height: 3.0),
              ),
              Container(
                child: Row(
                  children: [
                    Icon(Icons.volunteer_activism),
                    Expanded(
                        child: Container(
                      child: Text(
                        ' volunteer marathon Bangkok 2019',
                        style: TextStyle(fontSize: 12, height: 0),
                      ),
                    ))
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Icon(Icons.volunteer_activism),
                    Expanded(
                        child: Container(
                      child: Text(
                        ' volunteer Bangsaen21 2020',
                        style: TextStyle(fontSize: 12, height: 0),
                      ),
                    ))
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Icon(Icons.computer),
                    Expanded(
                        child: Container(
                      child: Text(
                        ' passed prepare co-operative education 15 hours',
                        style: TextStyle(fontSize: 12, height: 0),
                      ),
                    ))
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Icon(Icons.computer),
                    Expanded(
                        child: Container(
                      child: Text(
                        ' passed English for information technology course by Khon Kaen University 10 hours',
                        style: TextStyle(fontSize: 12, height: 0),
                      ),
                    ))
                  ],
                ),
              ),
            ],
          )),
        ],
      ),
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'resume_ployrung',
      home: Scaffold(
        appBar: AppBar(
          centerTitle: false,
          backgroundColor: Colors.blueAccent[200],
          title: const Text(
            'MY RESUME',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(18))),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.call,
                color: Colors.white,
              ),
              onPressed: () {
                // do something
              },
            )
          ],
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/IMG_4142.jpg',
              width: 200,
              height: 250,
              fit: BoxFit.contain,
            ),
            profileSection,
            educationSection,
            skillsSection,
            extraCurricularSection,
          ],
        ),
      ),
    );
  }
}
